﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    public partial class form_addUpdateMedia : UserControl
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        public event PropertyChangedEventHandler PropertyChanged;
        public string _numeMedia;
        public string numeMedia
        {
            get { return _numeMedia; }
            set
            {
                if (value != _numeMedia)
                {
                    _numeMedia = value;
                    OnPropertyChanged("numeMedia");
                }
            }
        }
        private string idMedia;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public form_addUpdateMedia(string in_idMedia)
        {
            InitializeComponent();
            DataContext = this;
            Background = Brushes.Transparent;
            idMedia = in_idMedia;
            if (idMedia != "0")
                numeMedia = god.get_media(idMedia);
        }

        private void SumbitThisModafaka(object sender, RoutedEventArgs e)
        {
            LetsDoThisShit();
        }

        private void SubmitFromTextBox(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            LetsDoThisShit();
        }

        private void LetsDoThisShit()
        {
            try
            {
                if (numeMedia.Length == 0) return;
                string q;
                if (idMedia == "0")
                    q = god.db_addMedia(numeMedia);
                else
                    q = god.db_updateMedia(idMedia, numeMedia);
                if (q == "1")
                {
                    _root.fly_.IsOpen = false;
                }
                else
                {
                    MessageBox.Show(q);
                }
            }
            catch (Exception) { }
        }
    }
}
