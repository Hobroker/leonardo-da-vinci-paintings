﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    /// <summary>
    /// Interaction logic for ItemTile.xaml
    /// </summary>
    public partial class ItemTile
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        public ItemTile(string idPictura, string nume, string poza)
        {
            InitializeComponent();
            Title = nume;
            Tag = idPictura;

            var b = new ImageBrush();
            Image i = new Image();
            i.Source = new BitmapImage(new Uri(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName + poza));
            b.ImageSource = i.Source;
            b.Stretch = Stretch.UniformToFill;
            Background = b;
            Click += delegate
            {
                _root.fly_paintingDetails.Content = new PaintingDetails(god.get_painting_details(idPictura));
                _root.fly_paintingDetails.Header = "Detaliile picturii";
                _root.fly_paintingDetails.IsOpen = true;
            };
        }
    }
}
