﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    /// <summary>
    /// Interaction logic for form_addTheme.xaml
    /// </summary>
    public partial class form_AddUpdateTheme : UserControl, INotifyPropertyChanged
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        public event PropertyChangedEventHandler PropertyChanged;
        public string _numeTema;
        public string numeTema
        {
            get { return _numeTema; }
            set
            {
                if (value != _numeTema)
                {
                    _numeTema = value;
                    OnPropertyChanged("numeTema");
                }
            }
        }
        private string idTema;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public form_AddUpdateTheme(string in_idTema)
        {
            InitializeComponent();
            DataContext = this;
            Background = Brushes.Transparent;
            idTema = in_idTema;
            if (idTema != "0")
                numeTema = god.get_theme(idTema);
        }

        private void SumbitThisModafaka(object sender, RoutedEventArgs e)
        {
            LetsDoThisShit();
        }

        private void SubmitFromTextBox(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            LetsDoThisShit();
        }

        private void LetsDoThisShit()
        {
            try
            {
                if (numeTema.Length == 0) return;
                string q;
                if (idTema == "0")
                    q = god.db_addTheme(numeTema);
                else
                    q = god.db_updateTheme(idTema, numeTema);
                if (q == "1")
                {
                    _root.fly_.IsOpen = false;
                }
                else
                {
                    Console.WriteLine(q);
                }
            }
            catch (Exception) { }
        }
    }
}
