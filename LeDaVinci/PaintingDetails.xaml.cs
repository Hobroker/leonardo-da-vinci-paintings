﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    public partial class PaintingDetails : UserControl
    {
        public PaintingDetails(Painting data)
        {
            InitializeComponent();
            nume.Text = data.nume;
            an.Text = data.an.ToString();
            x.Text = data.lungime + " X " + data.latime;
            numeTema.Text = data.idTema;
            numeMedia.Text = data.idMedia;
            numeLoc.Text = data.idLoc;
            descriere.Text = data.descriere;
            poza.Source = new BitmapImage(new Uri(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName + data.poza));
        }

        private void Btn_Export(object sender, RoutedEventArgs e)
        {
            ((Button)sender).Visibility = Visibility.Collapsed;
            var printDlg = new PrintDialog();
            printDlg.PrintVisual(this, nume.Text);
            ((Button)sender).Visibility = Visibility.Visible;
        }
    }
}
