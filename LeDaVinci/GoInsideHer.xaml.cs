﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    /// <summary>
    /// Interaction logic for GoInsideHer.xaml
    /// </summary>
    public partial class GoInsideHer : UserControl
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        private string usr
        {
            get
            {
                return in_usr.Text;
            }
        }
        private string psw
        {
            get
            {
                return in_psw.Password;
            }
        }
        public GoInsideHer()
        {
            InitializeComponent();
            Err.Visibility = Visibility.Hidden;
        }

        private void Btn_Submit(object sender, RoutedEventArgs e)
        {
            LetsDoThisShit();
        }

        private void LetsDoThisShit()
        {
            MainWindow.ImInsideHer = god.CanIPleaseGoInsideHer(usr, psw);
            if (!MainWindow.ImInsideHer)
                Err.Visibility = Visibility.Visible;
            else
            {
                _root.fly_login.IsOpen = false;
                _root.Nav.Navigate(_root.page_settings);
            }
        }

        private void SubmitFromTextBox(object sender, KeyEventArgs e)
        {
            if (Err.Visibility != Visibility.Hidden)
                Err.Visibility = Visibility.Hidden;
            if (e.Key != Key.Enter) return;
            LetsDoThisShit();
        }
    }
}
