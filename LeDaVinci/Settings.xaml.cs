﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    public partial class Settings : Page
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        public Settings()
        {
            InitializeComponent();
            Loaded += delegate
            {
                _root.RefreshThemes();
                _root.RefreshLocations();
                _root.RefreshMedias();
                _root.RefreshPaintings();
            };
        }

        public string idTema
        {
            get
            {
                try
                {
                    return ((ListBoxItem)data_themes.SelectedItem).Tag.ToString();
                }
                catch
                {
                    return "0";
                }
            }
        }

        public string idLoc
        {
            get
            {
                try
                {
                    return ((ListBoxItem)data_locations.SelectedItem).Tag.ToString();
                }
                catch
                {
                    return "0";
                }
            }
        }

        public string idMedia
        {
            get
            {
                try
                {
                    return ((ListBoxItem)data_medias.SelectedItem).Tag.ToString();
                }
                catch
                {
                    return "0";
                }
            }
        }

        public string idPictura
        {
            get
            {
                try
                {
                    return ((ListBoxItem)data_paintings.SelectedItem).Tag.ToString();
                }
                catch
                {
                    return "0";
                }
            }
        }

        private void Btn_AddTheme(object sender, RoutedEventArgs e)
        {
            _root.fly_.Content = new form_AddUpdateTheme("0");
            _root.fly_.Header = "Adaugă tematică";
            _root.fly_.IsOpen = true;
        }

        private void Btn_UpdateTheme(object sender, RoutedEventArgs e)
        {
            if (idTema == "0") return;
            _root.fly_.Content = new form_AddUpdateTheme(idTema);
            _root.fly_.Header = "Modifică tematică";
            _root.fly_.IsOpen = true;
        }

        private void Btn_DeleteTheme(object sender, RoutedEventArgs e)
        {
            if (idTema == "0") return;
            var result = god.db_deleteTheme(idTema);
            if (result != "1")
            {
                MessageBox.Show(result);
            }
        }

        private void Btn_AddLocation(object sender, RoutedEventArgs e)
        {
            _root.fly_.Content = new form_addUpdateLocation("0");
            _root.fly_.Header = "Adaugă locație";
            _root.fly_.IsOpen = true;
        }

        private void Btn_UpdateLocation(object sender, RoutedEventArgs e)
        {
            if (idLoc == "0") return;
            _root.fly_.Content = new form_addUpdateLocation(idLoc);
            _root.fly_.Header = "Modifică locație";
            _root.fly_.IsOpen = true;
        }

        private void Btn_DeleteLocation(object sender, RoutedEventArgs e)
        {
            if (idLoc == "0") return;
            var result = god.db_deleteLocations(idLoc);
            if (result != "1")
            {
                MessageBox.Show(result);
            }
        }

        private void Btn_AddMedia(object sender, RoutedEventArgs e)
        {
            _root.fly_.Content = new form_addUpdateMedia("0");
            _root.fly_.Header = "Adaugă media";
            _root.fly_.IsOpen = true;
        }

        private void Btn_UpdateMedia(object sender, RoutedEventArgs e)
        {
            if (idMedia == "0") return;
            _root.fly_.Content = new form_addUpdateMedia(idMedia);
            _root.fly_.Header = "Modifică media";
            _root.fly_.IsOpen = true;
        }

        private void Btn_DeleteMedia(object sender, RoutedEventArgs e)
        {
            if (idMedia == "0") return;
            var result = god.db_deleteMedia(idMedia);
            if (result != "1")
            {
                MessageBox.Show(result);
            }
        }

        private void Btn_AddPainting(object sender, RoutedEventArgs e)
        {
            _root.fly_.Content = new form_AddUpdatePainting("0");
            _root.fly_.Header = "Adaugă o pictură";
            _root.fly_.IsOpen = true;
        }

        private void Btn_UpdatePainting(object sender, RoutedEventArgs e)
        {
            if (idPictura == "0") return;
            _root.fly_.Content = new form_AddUpdatePainting(idPictura);
            _root.fly_.Header = "Modifică pictura";
            _root.fly_.IsOpen = true;
        }

        private void Btn_DeletePainting(object sender, RoutedEventArgs e)
        {
            if (idPictura == "0") return;
            var result = god.db_deletePainting(idPictura);
            if (result != "1")
            {
                MessageBox.Show(result);
            }
        }

        private void data_paintings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _root.fly_paintingDetails.Content = new PaintingDetails(god.get_painting_details(idPictura));
            _root.fly_paintingDetails.Header = "Detaliile picturii";
            _root.fly_paintingDetails.IsOpen = true;
        }
    }
}
