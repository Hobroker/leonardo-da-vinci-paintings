﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace LeDaVinci
{
    public partial class form_AddUpdatePainting : UserControl
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        string idLoc
        {
            get
            {
                try
                {
                    return ((ComboBoxItem)data_locations.SelectedItem).Tag.ToString();
                }
                catch (Exception)
                {
                    return "0";
                }
            }
        }
        string idMedia
        {
            get
            {
                try
                {
                    return ((ComboBoxItem)data_medias.SelectedItem).Tag.ToString();
                }
                catch (Exception)
                {
                    return "0";
                }
            }
        }
        string idTheme
        {
            get
            {
                try
                {
                    return ((ComboBoxItem)data_themes.SelectedItem).Tag.ToString();
                }
                catch (Exception)
                {
                    return "0";
                }
            }
        }
        private string idPictura;
        OpenFileDialog ofd = new OpenFileDialog
        {
            Title = "Alege o imagine",
            Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png"
        };
        public form_AddUpdatePainting(string _idPictura)
        {
            InitializeComponent();
            data_locations.ItemsSource = god.data_locations(false);
            data_medias.ItemsSource = god.data_medias(false);
            data_themes.ItemsSource = god.data_themes(false);
            idPictura = _idPictura;
            if (idPictura != "0")
            {
                var fuck = god.get_painting(idPictura);
                nume.Text = fuck.nume;
                an.Value = fuck.an;
                lungime.Value = fuck.lungime;
                latime.Value = fuck.latime;
                ofd.FileName = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName + fuck.poza;
                pozaPreview.Source = new BitmapImage(new Uri(ofd.FileName));
                //location
                foreach (ListBoxItem item in data_locations.Items)
                    if (item.Tag.ToString() == fuck.idLoc)
                    {
                        data_locations.SelectedItem = item;
                        break;
                    }
                //media
                foreach (ListBoxItem item in data_medias.Items)
                    if (item.Tag.ToString() == fuck.idMedia)
                    {
                        data_medias.SelectedItem = item;
                        break;
                    }
                //themes
                foreach (ListBoxItem item in data_themes.Items)
                    if (item.Tag.ToString() == fuck.idTema)
                    {
                        data_themes.SelectedItem = item;
                        break;
                    }

            }
        }

        private void SumbitThisModafaka(object sender, RoutedEventArgs e)
        {
            LetsDoThisShit();
        }

        private void LetsDoThisShit()
        {
            try
            {
                string q;
                var _path = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName + @"\imgs";
                Directory.CreateDirectory(_path);
                var fn = ofd.FileName;
                var newName = Path.GetFileName(fn);
                var extention = Path.GetExtension(fn);
                while (File.Exists(_path + @"\" + newName + extention))
                    newName += "_";
                File.Copy(fn, _path + @"\" + newName + extention);
                var data = new Painting
                {
                    nume = nume.Text,
                    an = Convert.ToInt32(an.Value),
                    lungime = Convert.ToInt32(lungime.Value),
                    latime = Convert.ToInt32(latime.Value),
                    idLoc = idLoc,
                    idTema = idTheme,
                    idMedia = idMedia,
                    descriere = descriere.Text,
                    poza = @"\imgs\" + newName + extention
                };
                if (idPictura == "0")
                    q = god.db_addPainting(data);
                else
                    q = god.db_updatePainting(idPictura, data);
                if (q == "1")
                    _root.fly_.IsOpen = false;
                else
                    MessageBox.Show(q);
            }
            catch (Exception) { }
        }

        private void Btn_BrowseImg(object sender, RoutedEventArgs e)
        {
            if (ofd.ShowDialog() == true)
                pozaPreview.Source = new BitmapImage(new Uri(ofd.FileName));
        }
    }
}
