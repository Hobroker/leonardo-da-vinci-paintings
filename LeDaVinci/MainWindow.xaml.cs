﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace LeDaVinci
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        static public SQLiteConnection con;
        public Settings page_settings;
        Main page_main;
        public static bool ImInsideHer;
        public MainWindow()
        {
            InitializeComponent();
            Loaded += delegate
            {
                ImInsideHer = false;
                db_connect();
                page_settings = new Settings();
                page_main = new Main("");
                Nav.Navigate(page_main);
            };
        }

        private void db_connect()
        {
            try
            {
                con = new SQLiteConnection(@"Data Source=data.db;Version=3;");
                con.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void RefreshThemes()
        {
            page_settings.data_themes.ItemsSource = god.data_themes(true);
        }

        public void RefreshLocations()
        {
            page_settings.data_locations.ItemsSource = god.data_locations(true);
        }

        public void RefreshMedias()
        {
            page_settings.data_medias.ItemsSource = god.data_medias(true);
        }

        public void RefreshPaintings()
        {
            page_settings.data_paintings.ItemsSource = god.data_paintings();
        }

        private void Btn_Link_Settings(object sender, RoutedEventArgs e)
        {
            if (ImInsideHer)
            {
                Nav.Navigate(page_settings);
                RefreshThemes();
                RefreshLocations();
                RefreshMedias();
                RefreshPaintings();
            }
            else
                fly_login.IsOpen = true;
        }

        private void Btn_Link_Main(object sender, RoutedEventArgs e)
        {
            Nav.Navigate(page_main);
            page_main.GetAllThePaintings();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            page_main = new Main(((TextBox)sender).Text);
            Nav.Navigate(page_main);
        }
    }
}
