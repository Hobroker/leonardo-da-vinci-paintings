﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using System.Data.SQLite;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using System.IO;

namespace LeDaVinci
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Page
    {
        public ObservableCollection<Tile> PaintingsItemsCollection { get; set; }
        private string key;
        public Main(string key)
        {
            InitializeComponent();
            this.key = key;
            Loaded += (sender, args) =>
            {
                err.Visibility = Visibility.Hidden;
                PaintingsItemsCollection = new ObservableCollection<Tile>();
                DataContext = this;
                GetAllThePaintings();
            };
        }

        public void GetAllThePaintings()
        {
            PaintingsItemsCollection.Clear();
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT idPictura, nume, poza FROM Picturi WHERE nume LIKE :key ORDER BY nume";
                cmd.Parameters.Add(new SQLiteParameter("key", "%" + key + "%"));
                try
                {
                    int q = 0;
                    var data = cmd.ExecuteReader();
                    while (data.Read())
                    {
                        q++;
                        PaintingsItemsCollection.Add(new ItemTile(data["idPictura"].ToString(), data["nume"].ToString(), data["poza"].ToString()));
                    }
                    if(q==0)
                        err.Visibility = Visibility.Visible;
                    data.Close();
                }
                catch (Exception r)
                {
                    MessageBox.Show(r.ToString());
                }
            }
        }
    }
}
