﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    public partial class SpecialListBoxItem
    {
        public SpecialListBoxItem(string idPictura, string out_nume, string out_lungime, string out_latime, string out_poza)
        {
            InitializeComponent();
            Tag = idPictura;
            nume.Text = out_nume;
            x.Text = out_lungime + " X " + out_latime;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(System.IO.Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName + out_poza, UriKind.Absolute);
            bitmap.EndInit();
            img.Source = bitmap;
        }
    }
}
