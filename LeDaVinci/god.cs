﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Controls;

namespace LeDaVinci
{
    class god
    {
        private static readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        public static string db_addTheme(string in_numeTema)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"INSERT INTO Tematici(numeTema) VALUES(:numeTema);";
                cmd.Parameters.Add(new SQLiteParameter("numeTema", in_numeTema));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshThemes();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }
        public static string db_addLocation(string in_numeLoc)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"INSERT INTO Locatie(numeLoc) VALUES(:numeLoc);";
                cmd.Parameters.Add(new SQLiteParameter("numeLoc", in_numeLoc));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshLocations();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_addMedia(string in_numeMedia)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"INSERT INTO Media(numeMedia) VALUES(:numeMedia);";
                cmd.Parameters.Add(new SQLiteParameter("numeMedia", in_numeMedia));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshMedias();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_addPainting(Painting in_painting)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"INSERT INTO Picturi(nume, descriere, poza, idLoc, idMedia, idTema, an, lungime, latime) VALUES (:nume, :descriere, :poza, :idLoc, :idMedia, :idTema, :an, :lungime, :latime)";
                cmd.Parameters.Add(new SQLiteParameter("nume", in_painting.nume));
                cmd.Parameters.Add(new SQLiteParameter("descriere", in_painting.descriere));
                cmd.Parameters.Add(new SQLiteParameter("poza", in_painting.poza));
                cmd.Parameters.Add(new SQLiteParameter("idLoc", in_painting.idLoc));
                cmd.Parameters.Add(new SQLiteParameter("idMedia", in_painting.idMedia));
                cmd.Parameters.Add(new SQLiteParameter("idTema", in_painting.idTema));
                cmd.Parameters.Add(new SQLiteParameter("an", in_painting.an));
                cmd.Parameters.Add(new SQLiteParameter("lungime", in_painting.lungime));
                cmd.Parameters.Add(new SQLiteParameter("latime", in_painting.latime));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshPaintings();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_updateTheme(string in_idTema, string in_numeTema)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"UPDATE Tematici SET numeTema=:numeTema WHERE idTema=:idTema;";
                cmd.Parameters.Add(new SQLiteParameter("numeTema", in_numeTema));
                cmd.Parameters.Add(new SQLiteParameter("idTema", in_idTema));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshThemes();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }
        public static string db_updateLocation(string in_idLoc, string in_numeLoc)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"UPDATE Locatie SET numeLoc=:numeLoc WHERE idLoc=:idLoc;";
                cmd.Parameters.Add(new SQLiteParameter("numeLoc", in_numeLoc));
                cmd.Parameters.Add(new SQLiteParameter("idLoc", in_idLoc));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshLocations();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }


        public static string db_updatePainting(string in_idPictura, Painting fuck)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"
UPDATE Picturi SET
nume = :nume,
an = :an,
lungime = :lungime,
latime = :latime,
idLoc = :idLoc,
idTema = :idTema,
idMedia = :idMedia,
descriere = :descriere,
poza = :poza
WHERE idPictura=:idPictura
";
                cmd.Parameters.Add(new SQLiteParameter("nume", fuck.nume));
                cmd.Parameters.Add(new SQLiteParameter("an", fuck.an));
                cmd.Parameters.Add(new SQLiteParameter("lungime", fuck.lungime));
                cmd.Parameters.Add(new SQLiteParameter("latime", fuck.latime));
                cmd.Parameters.Add(new SQLiteParameter("idLoc", fuck.idLoc));
                cmd.Parameters.Add(new SQLiteParameter("idTema", fuck.idTema));
                cmd.Parameters.Add(new SQLiteParameter("idMedia", fuck.idMedia));
                cmd.Parameters.Add(new SQLiteParameter("descriere", fuck.descriere));
                cmd.Parameters.Add(new SQLiteParameter("poza", fuck.poza));
                cmd.Parameters.Add(new SQLiteParameter("idPictura", in_idPictura));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshPaintings();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_updateMedia(string in_idMedia, string in_numeMedia)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"UPDATE Media SET numeMedia=:numeMedia WHERE idMedia=:idMedia;";
                cmd.Parameters.Add(new SQLiteParameter("numeMedia", in_numeMedia));
                cmd.Parameters.Add(new SQLiteParameter("idMedia", in_idMedia));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshMedias();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_deleteTheme(string in_idTema)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"DELETE FROM Tematici WHERE idTema=:idTema;";
                cmd.Parameters.Add(new SQLiteParameter("idTema", in_idTema));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshThemes();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_deleteLocations(string in_idLoc)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"DELETE FROM Locatie WHERE idLoc=:idLoc;";
                cmd.Parameters.Add(new SQLiteParameter("idLoc", in_idLoc));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshLocations();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_deleteMedia(string in_idMedia)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"DELETE FROM Media WHERE idMedia=:idMedia;";
                cmd.Parameters.Add(new SQLiteParameter("idMedia", in_idMedia));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshMedias();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static string db_deletePainting(string in_idPictura)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"DELETE FROM Picturi WHERE idPictura=:idPictura;";
                cmd.Parameters.Add(new SQLiteParameter("idPictura", in_idPictura));
                try
                {
                    cmd.ExecuteNonQuery();
                    _root.RefreshPaintings();
                    return "1";
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
        }

        public static List<ListBoxItem> data_themes(bool IsThisForListBox)
        {
            var data_out = new List<ListBoxItem>();
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT idTema, numeTema FROM Tematici";
                try
                {
                    var data = cmd.ExecuteReader();
                    while (data.Read())
                    {
                        if(IsThisForListBox)
                            data_out.Add(new TextListBoxItem(data["idTema"].ToString(), data["numeTema"].ToString()));
                        else
                            data_out.Add(new ComboBoxItem
                            {
                                Content = data["numeTema"].ToString(),
                                Tag = data["idTema"].ToString()
                            });
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
            return data_out;
        }
        public static List<ListBoxItem> data_locations(bool IsThisForListBox)
        {
            var data_out = new List<ListBoxItem>();
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT idLoc, numeLoc FROM Locatie";
                try
                {
                    var data = cmd.ExecuteReader();
                    while (data.Read())
                    {
                        if (IsThisForListBox)
                            data_out.Add(new TextListBoxItem(data["idLoc"].ToString(), data["numeLoc"].ToString()));
                        else
                            data_out.Add(new ComboBoxItem
                            {
                                Content = data["numeLoc"].ToString(),
                                Tag = data["idLoc"].ToString()
                            });
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
            return data_out;
        }
        public static List<ListBoxItem> data_medias(bool IsThisForListBox)
        {
            var data_out = new List<ListBoxItem>();
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT idMedia, numeMedia FROM Media";
                try
                {
                    var data = cmd.ExecuteReader();
                    while (data.Read())
                    {
                        if (IsThisForListBox)
                            data_out.Add(new TextListBoxItem(data["idMedia"].ToString(), data["numeMedia"].ToString()));
                        else
                            data_out.Add(new ComboBoxItem
                            {
                                Content = data["numeMedia"].ToString(),
                                Tag = data["idMedia"].ToString()
                            });
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
            return data_out;
        }

        public static List<ListBoxItem> data_paintings()
        {
            var data_out = new List<ListBoxItem>();
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT * FROM Picturi";
                try
                {
                    var data = cmd.ExecuteReader();
                    while (data.Read())
                    {
                        data_out.Add(new SpecialListBoxItem(data["idPictura"].ToString(), data["nume"].ToString(), data["lungime"].ToString(), data["latime"].ToString(), data["poza"].ToString()));
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
            return data_out;
        }

        public static string get_theme(string idTema)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT numeTema FROM Tematici WHERE idTema=:idTema";
                cmd.Parameters.Add(new SQLiteParameter("idTema", idTema));
                try
                {
                    var data = cmd.ExecuteReader();
                    return data["numeTema"].ToString();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return "Shit!";
                }
            }
        }
        public static string get_location(string idLoc)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT numeLoc FROM Locatie WHERE idLoc=:idLoc";
                cmd.Parameters.Add(new SQLiteParameter("idLoc", idLoc));
                try
                {
                    var data = cmd.ExecuteReader();
                    return data["numeLoc"].ToString();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return "Shit!";
                }
            }
        }

        public static string get_media(string idMedia)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT numeMedia FROM Media WHERE idMedia=:idMedia";
                cmd.Parameters.Add(new SQLiteParameter("idMedia", idMedia));
                try
                {
                    var data = cmd.ExecuteReader();
                    return data["numeMedia"].ToString();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return "Shit!";
                }
            }
        }

        public static Painting get_painting(string idPictura)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT * FROM Picturi WHERE idPictura=:idPictura";
                cmd.Parameters.Add(new SQLiteParameter("idPictura", idPictura));
                try
                {
                    var data = cmd.ExecuteReader();
                    return new Painting
                    {
                        nume = data["nume"].ToString(),
                        an = Convert.ToInt32(data["an"]),
                        lungime = Convert.ToInt32(data["lungime"]),
                        latime = Convert.ToInt32(data["latime"]),
                        idLoc = data["idLoc"].ToString(),
                        idTema = data["idTema"].ToString(),
                        idMedia = data["idMedia"].ToString(),
                        descriere = data["descriere"].ToString(),
                        poza = data["poza"].ToString()
                    };
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return new Painting();
                }
            }
        }

        public static Painting get_painting_details(string idPictura)
        {
            using (var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = @"
SELECT nume, an, lungime, latime, descriere, poza, numeTema, numeMedia, numeLoc
FROM Picturi P
INNER JOIN Locatie L ON L.idLoc = P.idLoc
INNER JOIN Media M ON M.idMedia = P.idMedia
INNER JOIN Tematici T ON T.idTema = P.idTema
WHERE idPictura=:idPictura
";
                cmd.Parameters.Add(new SQLiteParameter("idPictura", idPictura));
                try
                {
                    var data = cmd.ExecuteReader();
                    return new Painting
                    {
                        nume = data["nume"].ToString(),
                        an = Convert.ToInt32(data["an"]),
                        lungime = Convert.ToInt32(data["lungime"]),
                        latime = Convert.ToInt32(data["latime"]),
                        idLoc = data["numeLoc"].ToString(),
                        idTema = data["numeTema"].ToString(),
                        idMedia = data["numeMedia"].ToString(),
                        descriere = data["descriere"].ToString(),
                        poza = data["poza"].ToString()
                    };
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return new Painting();
                }
            }
        }

        public static bool CanIPleaseGoInsideHer(string usr, string psw)
        {
            using(var cmd = new SQLiteCommand(MainWindow.con))
            {
                cmd.CommandText = "SELECT count(*) boob FROM Users WHERE usr=:usr AND psw=:psw";
                cmd.Parameters.Add(new SQLiteParameter("usr", usr));
                cmd.Parameters.Add(new SQLiteParameter("psw", psw));
                try
                {
                    var data = cmd.ExecuteReader();
                    data.Read();
                    return data["boob"].ToString() == "1" ? true : false;
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.ToString());
                    return false;
                }
            }
        }
    }
    public class Painting
    {
        public string nume, descriere, poza, idLoc, idMedia, idTema;
        public int an, lungime, latime;
        
    }
}
