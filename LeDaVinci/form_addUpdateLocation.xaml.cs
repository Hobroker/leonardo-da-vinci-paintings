﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LeDaVinci
{
    /// <summary>
    /// Interaction logic for form_addUpdateLocation.xaml
    /// </summary>
    public partial class form_addUpdateLocation : UserControl
    {
        private readonly MainWindow _root = Application.Current.MainWindow as MainWindow;
        public event PropertyChangedEventHandler PropertyChanged;
        public string _numeLoc;
        public string numeLoc
        {
            get { return _numeLoc; }
            set
            {
                if (value != _numeLoc)
                {
                    _numeLoc = value;
                    OnPropertyChanged("numeLoc");
                }
            }
        }
        private string idLoc;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public form_addUpdateLocation(string in_idLoc)
        {
            InitializeComponent();
            DataContext = this;
            Background = Brushes.Transparent;
            idLoc = in_idLoc;
            if (idLoc != "0")
                numeLoc = god.get_location(idLoc);
        }

        private void SumbitThisModafaka(object sender, RoutedEventArgs e)
        {
            LetsDoThisShit();
        }

        private void SubmitFromTextBox(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            LetsDoThisShit();
        }

        private void LetsDoThisShit()
        {
            try
            {
                if (numeLoc.Length == 0) return;
                string q;
                if (idLoc == "0")
                    q = god.db_addLocation(numeLoc);
                else
                    q = god.db_updateLocation(idLoc, numeLoc);
                if (q == "1")
                {
                    _root.fly_.IsOpen = false;
                }
                else
                {
                    MessageBox.Show(q);
                }
            }
            catch (Exception) { }
        }
    }
}
